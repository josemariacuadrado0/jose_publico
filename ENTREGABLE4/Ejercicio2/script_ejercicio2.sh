#Separar la fecha en variables
YEAR=$(date +"%Y")
MONTH=$(date +"%m")
DAY=$(date +"%d")
DAYOFWEEK=$(date +"%u")

STUDENT="josemariacuadrado"

#Comprobar si existe el directorio, en caso contrario será creado
mkdir -p backup/$STUDENT/$YEAR/$MONTH/$DAY

#Copiar el txt y renombrar
cp resultado.txt backup/$STUDENT/$YEAR/$MONTH/$DAY/nginx_log_requests_$YEAR$MONTH$DAY.log

#Si es el ultimo dia de la semana, comprime los ficheros
if [ "$DAYOFWEEK" == 7 ];  then
        #Crear un directorio temporal para copiar los ficheros
        mkdir -p backup/$STUDENT/$YEAR/$MONTH/temp

        #Busca todos los ficheros .log y los copia
        find backup/$STUDENT/$YEAR/ -type f -mtime -7 |xargs cp -t backup/$STUDENT/$YEAR/$MONTH/temp

        #Comprimimos todos los ficheros del directorio
        tar -C backup/$STUDENT/$YEAR/$MONTH/temp -cf nginx_logs_$YEAR$MONTH$DAY.tar.gz .

        #Eliminamos la carpeta temporal
        rm -rf backup/$STUDENT/$YEAR/$MONTH/temp

        #Movemos el tar a la carpeta history
        mv nginx_logs_$YEAR$MONTH$DAY.tar.gz history/nginx_logs_$YEAR$MONTH$DAY.tar.gz
fi
